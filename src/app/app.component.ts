import { Component, Pipe } from '@angular/core';
import swal from 'sweetalert2';

// declare let swal: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor() {}

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {}

}
