import { Routes} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProductosComponent } from './components/productos/productos.component';
import { ProductComponent } from './components/product/product.component';
import { CartComponent } from './components/cart/cart.component';



export const APP_ROUTES: Routes = [
{path: 'home', component: HomeComponent},
{path: 'productos/:id', component: ProductosComponent},
{path: 'productos/:id/:last', component: ProductosComponent},
{path: 'producto/:id', component: ProductComponent},
{path: 'cart', component: CartComponent},
{ path: '**', pathMatch: 'full', redirectTo: 'home'}
];


