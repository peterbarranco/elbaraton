import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterializeModule } from 'angular2-materialize';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { OrderModule } from 'ngx-order-pipe';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { FormsModule } from '@angular/forms';

// Rutas
import { APP_ROUTES } from './app.routes';

// Componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SublevelsNavComponent } from './components/shared/sublevels-nav/sublevels-nav.component';
import { ProductosComponent } from './components/productos/productos.component';
import { ProductComponent } from './components/product/product.component';

// Servicios
import { CategoriesService } from './services/categories.service';
import { ProductsService } from './services/products.service';
import { CartComponent } from './components/cart/cart.component';








@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    SublevelsNavComponent,
    ProductosComponent,
    ProductComponent,
    CartComponent,
  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    HttpClientModule,
    OrderModule,
    FilterPipeModule,
    FormsModule,
    RouterModule.forRoot(APP_ROUTES, {useHash: true})
  ],
  providers: [
    CategoriesService,
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
