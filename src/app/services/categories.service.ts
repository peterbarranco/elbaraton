import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class CategoriesService {

    private categories: any;
    constructor( private http: HttpClient) {
        console.log('servicio listo para usarse');
    }

    getCategories() {
       return this.http.get('./src/app/data/categories.json');
    }
}

// export interface Categories {
//     id: number;
//     name: string;
//     sublevels?: number;
// }
