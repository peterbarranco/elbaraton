import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ProductosComponent } from '../components/productos/productos.component';

@Injectable()
export class ProductsService {
  products: any[];
  products_temp: any[];
  cart: any;
  constructor(private http: HttpClient) { }

  getProducts() {
    return this.http.get('./src/app/data/products.json');
  }

  getProductsById(idx: number) {
    this.products_temp = [];
    this.products = [];
    this.getProducts().subscribe((data: any) => {
      // console.log( data.products);
      this.products_temp = data.products;
         // se recorre el array para traer lo poductos con el sublevel_id seleccionado
      for (let i = 0; i < this.products_temp.length; i++) {

        const product = this.products_temp[i];
        const id = product.sublevel_id;
        // console.log(id);
        if (id.toString() === idx) {
          // console.log('Sublevel' + product.sublevel_id);
            this.products.push(product);
        }
      }
    });
    return this.products;
  }
  getCart () {
    // tslint:disable-next-line:prefer-const
    let cart = JSON.parse(localStorage.getItem('cart'));

    if (cart !== 'undefined') {
      this.cart = cart;
    } else {
      this.cart = null;
    }
    return this.cart;
  }



}
