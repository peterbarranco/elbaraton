import { Component, OnInit, DoCheck } from '@angular/core';
import {ProductsService} from '../../services/products.service';
import {Router} from '@angular/router';
import swal, { SweetAlertType } from 'sweetalert2';

// CONSTANTES AUXILIARES PARA UTILIZAR SWEET ALERT
const alert = swal.mixin({
  toast: false,
  position: 'center',
  showConfirmButton: false,
  timer: 3000
});

const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, DoCheck {
  public cart: any;
  public total: number;
  public title: string;

  constructor(private _productsService: ProductsService, private router: Router) {
    this.title = 'Carrito';
   }

  ngOnInit() {
    this.cart = this._productsService.getCart();
  }
  ngDoCheck() {
    this.totals();
  }

  // METODO PARA ELIMINAR PRODUCTOS DEL CARRITO
  deleteProduct(id) {
    this.cart.forEach((product, index) => {
      if (product.id === id ) {
        this.cart.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify(this.cart));
      }
    });
    toast({
      type: 'error',
      title: 'Producto eliminado'
    });
  }

  // METODO QUE ACTUALIZA LOS DATOS CUANDO CAMBIA LA CANTIDAD DE PRODUCTOS
  updateCant(id, event) {
    // tslint:disable-next-line:prefer-const
    let cant = event.target.value;
    this.cart.forEach((product, index) => {
      if (product.id === id ) {
        product.cant = cant;
      }
    });
    localStorage.setItem('cart', JSON.stringify(this.cart));
    this.totals();
    toast({
      type: 'success',
      title: 'Producto actualizado'
    });
  }

  // METODO QUE CALCULA EL TOTAL DE LA COMPRA
  totals() {
    // tslint:disable-next-line:prefer-const
    let suma = 0;
    this.cart.forEach((product, index) => {
      suma = (product.price * product.cant) + suma;
    });
    this.total = suma;
    console.log('suma');
  }


  // METODO QUE SIMULA EL PAGO
  pagar() {
    alert({
      title: 'Exelente!',
      text: 'su pago se proceso con exito',
      type: 'success',
      confirmButtonText: 'Cool'
    });
    this.cart = null;
    localStorage.clear();
    this.router.navigate(['/home']);
  }

}
