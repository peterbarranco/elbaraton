import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SublevelsNavComponent } from './sublevels-nav.component';

describe('SublevelsNavComponent', () => {
  let component: SublevelsNavComponent;
  let fixture: ComponentFixture<SublevelsNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SublevelsNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SublevelsNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
