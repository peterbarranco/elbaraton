import { Component, OnInit, DoCheck} from '@angular/core';
import { CategoriesService } from '../../../services/categories.service';
import { ProductsService } from '../../../services/products.service';


declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, DoCheck {

  categories: any[] = [];
  cart: any;
  last = true;
  constructor(private _categoriesService: CategoriesService, private _productService: ProductsService) {
    this._categoriesService.getCategories()
          .subscribe((data: any) => {
          console.log(data.categories);
          this.categories = data.categories;
    });
  }

  ngOnInit() {
    $('.button-collapse').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'left', // Choose the horizontal origin
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true, // Choose whether you can drag to open on touch screens,
      onOpen: function(el) { }, // A function to be called when sideNav is opened
      onClose: function(el) { }, // A function to be called when sideNav is closed
    }
  );

  }

  ngDoCheck() {
    this.cart = this._productService.getCart();
  }


  // getCategories(){
  //   this.categories = this._categoriesService.getCategories();
  //   retun
  // }

}
