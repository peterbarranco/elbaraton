import { Component, OnInit, DoCheck } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { ActivatedRoute, Params} from '@angular/router';
import { OrderPipe } from 'ngx-order-pipe';
import { FilterPipe } from 'ngx-filter-pipe';
import swal from 'sweetalert2';

// CONSTANTES PARA USAR SWEET ALERT
const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

// DECLARAR VARIABLE PARA EL USO DE JQUERY
declare var $;

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit, DoCheck {

  products: any[] = [];
  sortedProducts: any[];
  order: any = 'avaliable';
  filter_stock: any = { quantity: '' };
  filtro: any;
  valor: any = false;
  opcion: string;
  cart: any[];
  last: boolean;
  public title: string;

  constructor(private _productsService: ProductsService, private activateRoute: ActivatedRoute, private orderPipe: OrderPipe
    , private filterPipe: FilterPipe) {
      // this.sortedProducts = this.orderPipe.transform(this.products, this.order, this.valor);
      $(document).ready(function() {
        $('#ordenado').val('');
      });
      this.title = 'Tienda';
    }

  ngOnInit() {
    this.activateRoute.params.subscribe((params: Params) => {
      if (params['last']) {
        this.last = true;
      } else {
        this.last = false;
      }
      if (params['id']) {
        this.products = this._productsService.getProductsById(params['id']);
      } else {
        this._productsService.getProducts().subscribe(
          (response: any) => {
            this.products = response.products;
          }
        );
      }

    });

  }

  ngDoCheck() {

  }

  // METODO ENCARGADO DE ORDENAR LOS PRODUCTOS
  ordenar() {
    // tslint:disable-next-line:prefer-const
    let ordenado: string;
    // tslint:disable-next-line:prefer-const
    let valor: boolean;
    switch (this.opcion) {
      case 'available':
        ordenado = 'available';
        valor = true;
        break;
      case 'noavaliable':
        ordenado = 'available';
        valor = false;

        break;
      case 'quantity':
        ordenado = 'quantity';
        valor = true;
        break;
      case 'quantity2':
        ordenado = 'quantity';
        valor = false;
        break;
      case 'pricedes':
        ordenado = 'price';
        valor = true;
        break;
      case 'priceasc':
        ordenado = 'price';
        valor = false;
        break;

      default:
        ordenado = 'avaliable';
        valor = true;
        break;
    }
    this.order = ordenado;
    this.valor = valor;
  }


  // METODO ENCARGADO DE FILTRAR LOS PRODUCTOS
  filtrar(dato: string, event: any) {
    // console.log(event.target.value);
    switch (dato) {
      case 'stock':
          this.filtro = this.filter_stock;
        break;
      case 'disponible':
          this.filtro = {available : true};
        break;
      case 'nodisponible':
          this.filtro = {available : false};
        break;
      case 'todos':
          this.filtro = {quantity : ''};
        break;
      case 'name':
          this.filtro = {name : event.target.value};
        break;

      default:
        break;
    }
  }

  // METODO QUE ENVIA LOS PRODUCTOS AL CARRITO Y GUARDA EN LOCALSTORAGE

  sendtocart(id, name, price) {
    // tslint:disable-next-line:prefer-const
    let duplic = false;
    // tslint:disable-next-line:prefer-const
    let cart: any;
    this.cart = this._productsService.getCart();

    if (this.cart) {
      // Recorro el objeto y verifico si el producto que voy a agregar existe
      this.cart.forEach(product => {
        if (product.id === id) {
          duplic = true;
          product.cant ++;
        }
      });
      if (duplic === false) {
        cart = {
            id: id,
            name: name,
            price: price,
            cant: 1
          }
        ;
        this.cart.push(cart);
      }
    } else {
      cart = [
        {
          id: id,
          name: name,
          price: price,
          cant: 1
        }
      ];
      this.cart = cart;
    }
    // GUARDO EN LOCALSTORAGE
    localStorage.setItem('cart', JSON.stringify(this.cart));
    toast({
      type: 'success',
      title: 'Producto agregado al Carrito'
    });
  }
}
