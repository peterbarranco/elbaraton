import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import swal from 'sweetalert2';

// CONSTANTES PARA USAR SWEET ALERT
const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public title: string;
  public products: any[] = [];
  cart: any[];
  constructor(private _productsService: ProductsService ) {
    this.title = 'TU MERCADO COMO TU LO HARIAS';
  }

  ngOnInit() {
    this._productsService.getProducts().subscribe((reponse: any) => {
      this.products = reponse.products;
    });
  }

  sendtocart(id, name, price) {
    // tslint:disable-next-line:prefer-const
    let duplic = false;
    // tslint:disable-next-line:prefer-const
    let cart: any;
    this.cart = this._productsService.getCart();

    if (this.cart) {
      // Recorro el objeto y verifico si el producto que voy a agregar existe
      this.cart.forEach(product => {
        if (product.id === id) {
          duplic = true;
          product.cant ++;
        }
      });
      if (duplic === false) {
        cart = {
            id: id,
            name: name,
            price: price,
            cant: 1
          }
        ;
        this.cart.push(cart);
      }
    } else {
      cart = [
        {
          id: id,
          name: name,
          price: price,
          cant: 1
        }
      ];
      this.cart = cart;
    }
    // GUARDO EN LOCALSTORAGE
    localStorage.setItem('cart', JSON.stringify(this.cart));
    toast({
      type: 'success',
      title: 'Producto agregado al Carrito'
    });
  }

}
